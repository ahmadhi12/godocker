FROM golang:latest

RUN mkdir /build

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
# COPY go.mod .
# COPY go.sum .
# RUN go mod download

# Copy the code into the container
# COPY . .

# Build the application
# RUN go build -o main .

# Move to /dist directory as the place for resulting binary folder
# WORKDIR /dist

# Set necessary environmet variables needed for our image
RUN export GO111MODULE=on

# Copy binary from build to main folder
# RUN cp /build/main .
RUN cd /Docker/test/go_docker && go build

# Export necessary port
EXPOSE 3232

# Command to run when starting the container
# CMD ["/dist/main"]

ENTRYPOINT [ "/Docker/test/go_docker/main/main" ]