package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	Reques1()
}
func HomePage(response http.ResponseWriter, r *http.Request) {
	fmt.Printf("Welcome to golang web API!")
	fmt.Println("Endpoint Hit: HomePage")
}

func AboutMe(response http.ResponseWriter, r *http.Request) {
	who := "Ahmad"

	fmt.Fprintln(response, "A little bit about Ahmad...")
	fmt.Println("Endpoint Hit: ", who)
}

func Reques1() {
	http.HandleFunc("/", HomePage)
	http.HandleFunc("/about", AboutMe)
	log.Fatal(http.ListenAndServe(":3232", nil))
}
